using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpPowerUp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var player))
        {
            player.IncreaseAirJumps();
            SoundManager.Instance.PlaySound("Powerup");
            Destroy(gameObject);
        }
    }
}
