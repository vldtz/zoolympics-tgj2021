using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public Vector3 playerInput { get; private set; }

    #region Inspector Sliders
    [SerializeField, Range(0f, 100f)]
    private float maxSpeed = 10f;

    [SerializeField, Range(0f, 10f)]
    private float jumpHeight = 2f;

    [SerializeField, Range(0, 5)]
    private int maxAirJumps = 0;

    [SerializeField, Range(0f, 100f)]
    private float maxAcceleration = 10f, maxAirAcceleration = 1f;

    [SerializeField, Range(0f, 90f)]
    private float maxGroundAngle = 25f;

    [SerializeField]
    private Transform playerInputSpace = default;
    #endregion

    private bool desiredJump;
    private bool onGround;

    private Vector3 velocity, desiredVelocity;
   
    private int jumpPhase;
    private Rigidbody body;

    private Animator animator;

    private float minGroundDotProduct;

    private void OnValidate()
    {
        minGroundDotProduct = Mathf.Cos(maxGroundAngle * Mathf.Deg2Rad);
    }

    public void Awake()
    {
        body = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        OnValidate();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        playerInput = context.ReadValue<Vector2>();
        playerInput = Vector2.ClampMagnitude(playerInput, 1f);
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        desiredJump = context.action.triggered;
    }

    public void IncreaseAirJumps()
    {
        maxAirJumps++;
    }

    private void Update()
    {
        if (playerInputSpace)
        {
            desiredVelocity = playerInputSpace.TransformDirection(
                0f, 0f, playerInput.y
            ) * maxSpeed;
        }
        else
        {
            desiredVelocity = new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        }
        animator.SetBool("IsRunning", playerInput != Vector3.zero);
    }
    private void FixedUpdate()
    {
        UpdateState();
        float acceleration = onGround ? maxAcceleration : maxAirAcceleration;
        float maxSpeedChange = acceleration * Time.deltaTime;
        if (desiredJump)
        {
            desiredJump = false;
            Jump();
        }
        onGround = false;

        velocity.x = Mathf.MoveTowards(velocity.x, desiredVelocity.x, maxSpeedChange);
        velocity.z = Mathf.MoveTowards(velocity.z, desiredVelocity.z, maxSpeedChange);
        body.velocity = velocity;

        if (playerInput != Vector3.zero)
        {
            transform.right = GetComponentInChildren<OrbitCamera>().transform.right; // TODO: improve character rotation
        }
    }

    private void UpdateState()
    {
        velocity = body.velocity;
        if (onGround)
        {
            jumpPhase = 0;
        }
    }

    private void Jump()
    {
        if (onGround || jumpPhase < maxAirJumps)
        {
            jumpPhase += 1;
            float jumpSpeed = Mathf.Sqrt(-2f * Physics.gravity.y * jumpHeight);
            if (velocity.y > 0f)
            {
                jumpSpeed = Mathf.Max(jumpSpeed - velocity.y, 0f);
            }
            velocity.y += jumpSpeed;
            animator.SetTrigger("Jump");
            SoundManager.Instance.PlaySound("Jump");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bouncy")
            velocity *= -10f;
        EvaluateCollision(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        EvaluateCollision(collision);
    }

    private void EvaluateCollision(Collision collision)
    {
        for (int i = 0; i < collision.contactCount; i++)
        {
            Vector3 normal = collision.GetContact(i).normal;
            onGround |= normal.y >= minGroundDotProduct;
        }
    }
}