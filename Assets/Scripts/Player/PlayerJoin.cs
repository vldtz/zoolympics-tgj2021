using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


[RequireComponent(typeof(PlayerInputManager))]
public class PlayerJoin : MonoBehaviour
{
    public static Action<PlayerController> OnPlayerSpawned;

    [SerializeField]
    private GameObject playerPrefab;
    [SerializeField]
    private List<Transform> spawnLocations = new List<Transform>();
    private PlayerInputManager inputManager;
    private int index = 1;

    public IEnumerator Start()
    {
        var player1 = PlayerInput.Instantiate(playerPrefab, controlScheme: "Primary", pairWithDevice: Keyboard.current);
        SetupPlayer(player1);

        yield return new WaitForSeconds(0.1f); // enforce a small delay between players to ensure safe device pairing

        var player2 = PlayerInput.Instantiate(playerPrefab, controlScheme: "Secondary", pairWithDevice: Keyboard.current);
        SetupPlayer(player2);

        inputManager = GetComponent<PlayerInputManager>();
        inputManager.splitScreen = true;
        inputManager.DisableJoining();
    }

    private void SetupPlayer(PlayerInput player)
    {
        player.transform.localPosition = spawnLocations[index - 1].position;
        player.transform.localRotation = spawnLocations[index - 1].rotation;
        player.name = "Player " + index;
        OnPlayerSpawned.Invoke(player.GetComponent<PlayerController>());
        index++;
    }
}
