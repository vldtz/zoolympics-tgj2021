using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HUD : MonoBehaviour
{
    private Dictionary<PlayerController, ScorePanel> scorePanels = new Dictionary<PlayerController, ScorePanel>();
    private Queue<ScorePanel> scores = new Queue<ScorePanel>();
    private TimerPanel timerDisplay;
    private WinPanel winnerDisplay;

    private List<UIScreen> screens = new List<UIScreen>();

    private void UpdateScore(PlayerController player, int scoreToDisplay)
    {
        scorePanels[player].DisplayValue(scoreToDisplay);
    }

    private void UpdateTimer(string text)
    {
        timerDisplay.DisplayValue(text);
    }

    public void DisplayWinner(string winner)
    {
        winnerDisplay.DisplayValue(winner + " won!");
        ShowScreen<WinScreen>();
    }

    public void Start()
    {
        foreach (var scorePanel in GetComponentsInChildren<ScorePanel>())
            scores.Enqueue(scorePanel);
        timerDisplay = GetComponentInChildren<TimerPanel>(true);
        screens = GetComponentsInChildren<UIScreen>(true).ToList();
        winnerDisplay = GetComponentInChildren<WinPanel>(true);

        ScoreManager.OnScoreChanged += UpdateScore;
        Timer.onTimerChanged += UpdateTimer;
        PlayerJoin.OnPlayerSpawned += AddPlayer;
        GameManager.onGameFinished += DisplayWinner;
        GameManager.onGamePaused += ShowScreen<PauseScreen>;
        GameManager.onGameResumed += ShowScreen<GameScreen>;

        ShowScreen<GameScreen>();
    }

    private void AddPlayer(PlayerController player)
    {
        scorePanels.Add(player, scores.Dequeue());
    }

    private void ShowScreen<T>()
    {
        foreach (var screen in screens)
        {
            screen.gameObject.SetActive(screen.GetType() == typeof(T));
        }
    }

    private void OnDestroy()
    {
        ScoreManager.OnScoreChanged -= UpdateScore;
        Timer.onTimerChanged -= UpdateTimer;
        PlayerJoin.OnPlayerSpawned -= AddPlayer;
        GameManager.onGameFinished -= DisplayWinner;
        GameManager.onGamePaused -= ShowScreen<PauseScreen>;
        GameManager.onGameResumed -= ShowScreen<GameScreen>;

        scores.Clear();
        scorePanels.Clear();
    }
}
