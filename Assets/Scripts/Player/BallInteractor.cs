using UnityEngine;
using UnityEngine.InputSystem;

public class BallInteractor : MonoBehaviour
{
    private InteractableBall nearbyBall;
    private InteractableBall equippedBall;
    private Transform equipSlot;
    private PlayerController player;

    private void Start()
    {
        equipSlot = GetComponentInChildren<EquipSlot>().transform;
        player = GetComponent<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<InteractableBall>(out var interactable))
            nearbyBall = interactable;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<InteractableBall>(out var interactable))
            nearbyBall = null;
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            if (equippedBall != null)
            {
                equippedBall.SetShooter(player);
                equippedBall.OnUnequip();
                equippedBall = null;
                nearbyBall = equippedBall;
            }
            if (nearbyBall != null)
            {
                nearbyBall.OnEquip();
                equippedBall = nearbyBall;
                equippedBall.GetInteractable().transform.SetParent(equipSlot);
                equippedBall.GetInteractable().transform.localPosition = Vector3.zero;
                nearbyBall = null;
            }
        }
    }
}
