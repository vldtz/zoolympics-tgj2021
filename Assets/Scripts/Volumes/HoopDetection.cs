using UnityEngine;

public class HoopDetection : DetectionVolume
{
    public ScoreManager scoreManager;
    public bool isHoopActive = true;

    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (other.TryGetComponent<InteractableBall>(out var ball))
        {
            if (isHoopActive)
                scoreManager.ModifyScore(ball.shooter);
            SoundManager.Instance.PlaySound("Score");
        }
    }
}
