using UnityEngine;

public class DeactivationVolume : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<InteractableBall>(out var ball))
        {
            BallPool.Instance.ReturnToPool(ball);
        }
    }
}
