using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class DetectionVolume : MonoBehaviour
{

    [SerializeField]
    private UnityEvent onEnter = default;
    [SerializeField]
    private UnityEvent onExit = default;

	private List<Collider> colliders = new List<Collider>();

	private void Awake()
	{
		enabled = false;
	}

	private void FixedUpdate()
	{
		for (int i = 0; i < colliders.Count; i++)
		{
			Collider collider = colliders[i];
			if (!collider || !collider.gameObject.activeInHierarchy)
			{
				colliders.RemoveAt(i--);
				if (colliders.Count == 0)
				{
					onExit.Invoke();
					enabled = false;
				}
			}
		}
	}

	public virtual void OnTriggerEnter(Collider other)
	{
		if (colliders.Count == 0)
		{
			onEnter.Invoke();
			enabled = true;
		}
		colliders.Add(other);
	}

	private void OnTriggerExit(Collider other)
	{
		if (colliders.Remove(other) && colliders.Count == 0)
		{
			onExit.Invoke();
			enabled = false;
		}
	}

	private void OnDisable()
	{
		if (colliders.Count > 0)
		{
			colliders.Clear();
			onExit.Invoke();
		}
	}
}