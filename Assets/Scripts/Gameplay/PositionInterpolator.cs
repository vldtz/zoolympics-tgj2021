using UnityEngine;

public class PositionInterpolator : MonoBehaviour
{

    public Transform[] Positions;
    public float Speed = 2f;
    private int currentIndex;

    private void Start()
    {
        currentIndex = 0;
        transform.position = GetCurrentTarget();
    }

    private Vector3 GetCurrentTarget()
    {
        if (Positions.Length == 0)
            return transform.position;
        return Positions[currentIndex].position;
    }

    private void Update()
    {
        var targetPosition = GetCurrentTarget() - transform.position;
        var moveDir = targetPosition.normalized * Speed * Time.deltaTime;
        if (targetPosition.magnitude < 0.05f || moveDir.magnitude > targetPosition.magnitude)
        {
            if (++currentIndex == Positions.Length)
            {
                currentIndex = 0;
            }
            moveDir = targetPosition;
        }
        transform.position += moveDir;
    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.SetParent(transform);
    }

    private void OnTriggerExit(Collider other)
    {
        other.transform.SetParent(null);
    }
}