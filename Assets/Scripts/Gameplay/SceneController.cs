using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    public void LoadSceneWithIndex(int index)
    {
        SceneManager.LoadScene(index);
    }
}
