using UnityEngine;

public class ImpactReceiver : MonoBehaviour
{
    private float mass = 3f;
    private Vector3 impact = Vector3.zero;
    private CharacterController characterController;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    public void AddImpact(Vector3 direction, float force)
    {
        direction.Normalize();
        if (direction.y < 0)
            direction.y = -direction.y;
        impact += direction.normalized * force / mass;
    }

    private void Update()
    {
        if (impact.magnitude > 0.2) characterController.Move(impact * Time.deltaTime);
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
    }
}
