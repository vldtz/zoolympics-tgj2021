using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static Action<PlayerController, int> OnScoreChanged;

    private Dictionary<PlayerController, int> playerScores = new Dictionary<PlayerController, int>();

    private void Awake()
    {
        PlayerJoin.OnPlayerSpawned += AddPlayerToScores;
    }

    private void OnDestroy()
    {
        PlayerJoin.OnPlayerSpawned -= AddPlayerToScores;
    }

    private void AddPlayerToScores(PlayerController player)
    {
        playerScores.Add(player, 0);
    }

    public void ModifyScore(PlayerController player)
    {
        playerScores[player] += 1;
        OnScoreChanged.Invoke(player, playerScores[player]);
    }

    public string GetWinnerName()
    {
        bool allScoresEqual = playerScores.Values.Distinct().Count() == 1;
        if (allScoresEqual)
        {
            return "Both players";
        }
        else
        {
            return playerScores.Aggregate((l, r) => l.Value > r.Value ? l : r).Key.name;
        }
    }
}
