using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float speed = 15f;
    private void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
    }
}
