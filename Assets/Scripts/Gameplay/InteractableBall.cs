using System;
using UnityEngine;

public class InteractableBall : MonoBehaviour, IInteractable
{
    public PlayerController shooter { get; private set; }
    private Rigidbody body;

    public GameObject GetInteractable()
    {
        return gameObject;
    }

    private void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    public void OnEquip()
    {
        body.velocity = Vector3.zero;
        body.isKinematic = true;
    }

    public void OnUnequip()
    {
        transform.localRotation.eulerAngles.Set(0f, 0f, 0f);
        body.isKinematic = false;
        body.AddForce(shooter.transform.forward * 20f, ForceMode.Impulse);
        body.angularVelocity = UnityEngine.Random.insideUnitSphere * 5f;
        transform.parent = null;
        SoundManager.Instance.PlaySound("Kick");
    }

    public void SetShooter(PlayerController player)
    {
        shooter = player;
    }

    private void OnDisable()
    {
        ResetVelocity();
    }

    public void ResetVelocity()
    {
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
    }
}
