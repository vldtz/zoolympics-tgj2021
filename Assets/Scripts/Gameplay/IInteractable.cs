using UnityEngine;

public interface IInteractable 
{
    public void OnEquip();
    public void OnUnequip();

    public GameObject GetInteractable();
}
