using UnityEngine;

public class BallPool : GenericPool<InteractableBall>
{
    public override void SpawnAtPoint(Transform spawnPoint)
    {
        base.SpawnAtPoint(spawnPoint);
        SoundManager.Instance.PlaySound("Magic");
    }
}
