﻿using System.Collections.Generic;
using UnityEngine;

public abstract class GenericPool<T> : MonoBehaviour where T : Component
{
	[SerializeField]
	private T prefab;
	public static GenericPool<T> Instance { get; private set; }
	private Queue<T> objects = new Queue<T>();

	private void Awake()
	{
		Instance = this;
	}

	public T GetPooledObject(bool spawnInactive = true)
	{
		if (objects.Count == 0)
		{
			AddNewObjects(1, spawnInactive);
		}
		return objects.Dequeue();
	}

	public void ReturnToPool(T objectToReturn)
	{
		objectToReturn.gameObject.SetActive(false);
		objects.Enqueue(objectToReturn);
	}

	public void AddNewObjects(int count, bool spawnInactive)
	{
		for (int i = 0; i < count; i++)
		{
			var newObject = GameObject.Instantiate(prefab);
			newObject.gameObject.SetActive(!spawnInactive);
			objects.Enqueue(newObject);
		}
	}

	public virtual void SpawnAtPoint(Transform spawnPoint)
    {
		var pooledObject = GetPooledObject(false);
		pooledObject.gameObject.SetActive(true);
		pooledObject.transform.position = spawnPoint.position;
    }
}
