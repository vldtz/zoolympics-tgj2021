using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Action onGamePaused;
    public static Action onGameResumed;
    public static Action<string> onGameFinished;

    private bool isGamePaused = false;
    private ScoreManager scoreManager;

    private void Start()
    {
        scoreManager = GetComponent<ScoreManager>();
    }

    public void TogglePause()
    {
        if (isGamePaused)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    private void PauseGame()
    {
        isGamePaused = true;
        Time.timeScale = 0f;
        onGamePaused.Invoke();
    }

    private void ResumeGame()
    {
        isGamePaused = false;
        Time.timeScale = 1f;
        onGameResumed.Invoke();
    }

    public void SetWinner()
    {
        onGameFinished.Invoke(scoreManager.GetWinnerName());
        isGamePaused = true;
        Time.timeScale = 0f;
        SoundManager.Instance.PlaySound("Finish");
    }

    private void OnDestroy()
    {
        isGamePaused = false;
        Time.timeScale = 1f;
    }
}
