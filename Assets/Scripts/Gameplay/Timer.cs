using System;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public static Action<string> onTimerChanged;

    [SerializeField]
    private float timeRemaining = 10f;
    [SerializeField]
    private UnityEvent onTimerEnd = default;
    private bool isRunning = false;

    private void Start()
    {
        isRunning = true;
    }

    private void Update()
    {
        if (!isRunning)
            return;

        if (timeRemaining > 0f)
        {
            timeRemaining -= Time.deltaTime;
            GetFormattedTime(timeRemaining);
            onTimerChanged.Invoke(GetFormattedTime(timeRemaining));
        }
        else
        {
            onTimerEnd.Invoke();
            timeRemaining = 0f;
            isRunning = false;
        }
    }

    private string GetFormattedTime(float timeToDisplay)
    {
        timeToDisplay += 1;
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}