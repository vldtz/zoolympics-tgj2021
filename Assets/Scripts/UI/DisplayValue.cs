using TMPro;
using UnityEngine;

public class DisplayValue : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    public void SetText(string content)
    {
        text.text = content;
    }
}
