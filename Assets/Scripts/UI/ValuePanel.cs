using UnityEngine;

public class ValuePanel : MonoBehaviour
{
    private DisplayValue display;

    private void Awake()
    {
        display = GetComponentInChildren<DisplayValue>();
        display.SetText("");
    }


    public void DisplayValue(int value)
    {
        display.SetText(value.ToString());
    }

    public void DisplayValue(string value)
    {
        display.SetText(value);
    }
}
