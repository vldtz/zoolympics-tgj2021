using UnityEngine;
using UnityEngine.UI;

public class SoundToggle : MonoBehaviour
{
    private Toggle toggle;

    private void Awake()
    {
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener((toggleValue) => { SoundManager.Instance.ToggleSounds(!toggleValue); });
    }
}
