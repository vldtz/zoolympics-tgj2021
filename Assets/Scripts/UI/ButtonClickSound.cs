using UnityEngine;
using UnityEngine.UI;

public class ButtonClickSound : MonoBehaviour
{
    private Button[] childButtons;

    private void Awake()
    {
        childButtons = GetComponentsInChildren<Button>(true);
        foreach (var button in childButtons)
            button.onClick.AddListener(() => SoundManager.Instance.PlaySound("ButtonClick"));
    }

    private void OnDestroy()
    {
        foreach (var button in childButtons)
            button.onClick.RemoveAllListeners();
    }
}
