﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get { return instance; } }
    private SoundLoader soundLoader;

    private AudioSource musicSource;
    private AudioSource sfxSource;

    private static SoundManager instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this);
    }

    public void Start()
    {        
        soundLoader = new SoundLoader();
        SetupAudioSources();
        PlayMusic("Music");
    }

    private void SetupAudioSources()
    {
        musicSource = gameObject.AddComponent<AudioSource>();
        sfxSource = gameObject.AddComponent<AudioSource>();
        musicSource.loop = true;
        sfxSource.loop = false;
        musicSource.volume = 0.5f;
    }

    public void PlayMusic(string id)
    {
        var musicClip = soundLoader.GetClip(id);

        musicSource.clip = musicClip;
        musicSource.Play();
    }

    private void SetSFXVolume(float volume)
    {
        volume = Mathf.Clamp(volume, 0.0001f, 0.5f);
        sfxSource.volume = volume;
    }

    private void SetMusicVolume(float volume)
    {
        volume = Mathf.Clamp(volume, 0.0001f, 1f);
        musicSource.volume = volume;
    }

    public void PlaySound(string id)
    {
        var sfxClip = soundLoader.GetClip(id);
        sfxSource.PlayOneShot(sfxClip);
    }

    public void ToggleSounds(bool isMuted)
    {
        SetMusicVolume(isMuted ? 0f : 0.5f);
        SetSFXVolume(isMuted ? 0f : 1f);
    }
}
