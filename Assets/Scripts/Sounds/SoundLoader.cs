﻿using System.Collections.Generic;
using UnityEngine;

public class SoundLoader
{
    Dictionary<string, AudioClip> ClipLibrary = new Dictionary<string, AudioClip>();

    private void LoadDefinitions()
    {
        var gameSounds = Resources.Load<GameSounds>("GameSounds");

        foreach (var clip in gameSounds.audioClips)
        {
            ClipLibrary.Add(clip.Name, clip.Clip);
        }
    }

    public SoundLoader()
    {
        LoadDefinitions();
    }

    public AudioClip GetClip(string id)
    {
        if (ClipLibrary.TryGetValue(id, out var clip))
        {
            return clip;
        }
        else
        {
            Debug.LogError(" No clip named: " + id);
            return null;
        }
    }
}
