﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSounds", menuName = "Zoolympics/Game Sounds")]
public class GameSounds : ScriptableObject
{
	public List<AudioClipDefinition> audioClips;
}

[Serializable]
public class AudioClipDefinition
{
    public string Name;
    public AudioClip Clip;
}

